function [X_norm, mu, sigma] = featureNormalize(X)
%FEATURENORMALIZE Normalizes the features in X 
%   FEATURENORMALIZE(X) returns a normalized version of X where
%   the mean value of each feature is 0 and the standard deviation
%   is 1. This is often a good preprocessing step to do when
%   working with learning algorithms.

% You need to set these values correctly
X_norm = X;
mu = zeros(1, size(X, 2));    % mu = 1* columns of X
sigma = zeros(1, size(X, 2)); % sigma = 1* columns of X

% ====================== YOUR CODE HERE ======================
% Instructions: First, for each feature dimension, compute the mean
%               of the feature and subtract it from the dataset,
%               storing the mean value in mu. Next, compute the 
%               standard deviation of each feature and divide
%               each feature by it's standard deviation, storing
%               the standard deviation in sigma. 
%
%               Note that X is a matrix where each column is a 
%               feature and each row is an example. You need 
%               to perform the normalization separately for 
%               each feature. 
%
% Hint: You might find the 'mean' and 'std' functions useful.
%       

 
  mu = mean(X);
  sigma = std(X);  
  
  X_feature_1 = (X(:,1) - mu(:,1)) / sigma(:,1);
  X_feature_2 = (X(:,2) - mu(:,2)) / sigma(:,2);
  
  X_norm = [X_feature_1, X_feature_2] ;
%for featureIndex = 1:length(X)
  
%   X_feature_1 = (X(:,featureIndex) - mu(:,featureIndex)) / sigma(:,featureIndex)
   
%   X_norm
%    feature_transpose = X'(:featureIndex) - mu
    
    %norm_feature = X(:,featureIndex) - mean(X(:,featureIndex)) / std(X(:,featureIndex));
    %X_norm = X_norm norm_feature
    
    %for j = 1:length(theta1_vals)
	  %t = [theta0_vals(i); theta1_vals(j)];    
	  %J_vals(i,j) = computeCost(X, y, t);
    %end
%end

% ============================================================

end
