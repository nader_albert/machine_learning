function [J, grad] = lrCostFunction(theta, X, y, lambda)
%LRCOSTFUNCTION Compute cost and gradient for logistic regression with 
%regularization
%   J = LRCOSTFUNCTION(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta
%
% Hint: The computation of the cost function and gradients can be
%       efficiently vectorized. For example, consider the computation
%
%           sigmoid(X * theta)
%
%       Each row of the resulting matrix will contain the value of the
%       prediction for that example. You can make use of this to vectorize
%       the cost function and gradient computations. 
%
% Hint: When computing the gradient of the regularized cost function, 
%       there're many possible vectorized solutions, but one solution
%       looks like:
%           grad = (unregularized gradient for logistic regression)
%           temp = theta; 
%           temp(1) = 0;   % because we don't add anything for j = 0  
%           grad = grad + YOUR_CODE_HERE (using the temp variable)
%

% theta is 1 * n
% X is given as m * n; where the training examples are 'm' and the features are 'n' 
% so it's transpose will be n * m (features * examples)

X = X'; % n*m
H = theta' * X; % H is 1 * m

sigmoidOfH = sigmoid(H)'; %  m * 1

j1 = ( (- y') * log(sigmoidOfH)); % y = (1 * m) and log(sigmoidOfH) is (m *1), so the result is (1 * 1)

j2 = ( (1 - y') * log (1 - sigmoidOfH) ); %y = (1 * m) and log(sigmoidOfH) is (m *1), so the result is (1 * 1)

unnregularizedCost =  (j1 - j2) / m ; %1*1 

J = unnregularizedCost + (lambda / (2 * m)) * sum((theta(2:end,:) ).^2); % exclude the first theta row, which actually corresponds to theta 0, which sshouldn't be regularized 

grad1 = (sigmoidOfH - y); %m * 1

grad = (X * grad1) / m  ;% X is (n * m) and grad1 is (m * 1) so the result must be (n * 1), which represents a vector where each element is the grad value for a specific theta

% =============================================================

for thetaJ = 2:size(grad)
   grad(thetaJ) = grad(thetaJ) + (lambda/m) * theta(thetaJ);
end

grad = grad(:);

end
