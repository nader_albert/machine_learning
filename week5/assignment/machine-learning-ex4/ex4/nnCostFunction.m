function [J grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices. 
% 
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%

% Reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
% for our 2 layer neural network
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

% Setup some useful variables
m = size(X, 1);
         
% You need to return the following variables correctly 
J = 0;
Theta1_grad = zeros(size(Theta1));
Theta2_grad = zeros(size(Theta2));

% ====================== YOUR CODE HERE ======================
% Instructions: You should complete the code by working through the
%               following parts.
%
% Part 1: Feedforward the neural network and return the cost in the
%         variable J. After implementing Part 1, you can verify that your
%         cost function computation is correct by verifying the cost
%         computed in ex4.m

X = [ones(m, 1) X]; % 5000 * 401

% ========================== IMPLEMENTATION 1 =============================
totalCost = 0 ;
singleLabelValue = 0;

for exampleIndex = 1: m
  singleExampleInputData = X(exampleIndex, 1:end) ;
  singleLabel = y(exampleIndex, 1:end) ;
  
  singleLabelValue = singleLabel;
 
  singleExampleReferenceLabels = zeros(num_labels,1);
  
  singleExampleReferenceLabels(singleLabelValue,1) = 1;
  
  %singleExampleReferenceLabels = singleExampleReferenceLabels'; % 10 * 1 ==> 1 * 10

  z2 = Theta1 * singleExampleInputData' ; % (25 * 401) * (401 * 1) = 25 * 1
  a2 = sigmoid(z2); % 25 * 1
  a2 = [1 ; a2]; % 26 * 1
  
  z3 = Theta2 * a2 ; % (10 * 26) * (26 * 1) = (10 * 1)
  H = sigmoid(z3); %10 * 1

  j1 = ( (- singleExampleReferenceLabels) .* log (H)) ; % (10 * 1) .* (10 * 1) = 10 * 1
      
  j2 = (1 - singleExampleReferenceLabels) .* log(1 - H); % 1 * 1  This is the 1 - y  
  
  exampleCost = j1 - j2 ;
    
  sum(exampleCost);
  
  totalCost += sum(exampleCost); %1*1 
  
 % ============================ Gradient Calculation =============================== 
 delta_3 = (H - singleExampleReferenceLabels);
 z2 = [1 ; z2]; % 26 * 1
  
 delta_2 = (Theta2' * delta_3) .* sigmoidGradient(z2); % Theta2 (10 * 26) , delta_3 (10 * 1)
 delta_2 = delta_2(2:end); % 25 * 1
 
 Theta2_grad += delta_3 * a2';
 Theta1_grad += delta_2 * singleExampleInputData;
end


%Theta1WithLambdaEffect = Theta1(:,2:end) .* (lambda / m);
%Theta2WithLambdaEffect = Theta2(:,2:end) .* (lambda / m);

%Theta1WithLambdaEffect = [Theta1(:,1) Theta1WithLambdaEffect];
%Theta2WithLambdaEffect = [Theta2(:,1) Theta2WithLambdaEffect];

%size(Theta1WithLambdaEffect)

%Theta1WithLambdaEffect = [Theta1(1,:) ] 

Theta1_grad = Theta1_grad .* (1.0 / m); %+ Theta1_grad;
Theta2_grad = Theta2_grad .* (1.0 / m); %+ Theta2_grad;

% account for the regularization parameter

Theta1_grad(:,2:end) = Theta1_grad(:,2:end) + Theta1(:,2:end) .* lambda / m ; 
Theta2_grad(:,2:end) = Theta2_grad(:,2:end) + Theta2(:,2:end) .* lambda / m ; 

unregularizedCost = 1.0 ./ m * totalCost;

lambdaEffect = lambda / (2 * m);
sigmaTheta1 = sum((sum(Theta1(:,2:end).^2))); % each sum represents one Sigma, the first 'sum' adds the rows together which results in one row vector and the second 'sum' adds the columns together which results in a scalar number
sigmaTheta2 = sum((sum(Theta2(:,2:end).^2))); % each sum represents one Sigma 

J = unregularizedCost + lambdaEffect * (sigmaTheta1 + sigmaTheta2);

% ========================== IMPLEMENTATION 2 =============================
% X = X'; % 401 * 5000

% Theta1 is 25 * 401
% X is now 401 * 5000 
% z1 = Theta1 * X ; 

% firstLayerActivation = sigmoid(z1); % 25 * 5000

% firstLayerActivation = [ones(1,m) ; firstLayerActivation]; % 26 * 5000

% secondLayerActivation = sigmoid(Theta2 * firstLayerActivation); %10 * 5000

% secondLayerActivation = secondLayerActivation'; % to make it one row for each example.. (5000 * 10)

% y = y' ; % y' is 1 * 5000

% j1 = - y * log (secondLayerActivation) ; % (1 * 5000) * (5000 * 10)  = (1 * 10)

%size(secondLayerActivation);

% oneMinusY = ones(num_labels,1) - y ;

% oneMinusSecondLayerActivation = ones(m,1) - secondLayerActivation ;

% size(oneMinusSecondLayerActivation)

% j2 = ( oneMinusY * log(oneMinusSecondLayerActivation) );

% j2 = (1 .- y ) * log (1 .- secondLayerActivation); % = (1 * 10)

% J =  sum ( (j1 - j2)) / m ; %1*1 

%
% Part 2: Implement the backpropagation algorithm to compute the gradients
%         Theta1_grad and Theta2_grad. You should return the partial derivatives of
%         the cost function with respect to Theta1 and Theta2 in Theta1_grad and
%         Theta2_grad, respectively. After implementing Part 2, you can check
%         that your implementation is correct by running checkNNGradients
%
%         Note: The vector y passed into the function is a vector of labels
%               containing values from 1..K. You need to map this vector into a 
%               binary vector of 1's and 0's to be used with the neural network
%               cost function.
%
%         Hint: We recommend implementing backpropagation using a for-loop
%               over the training examples if you are implementing it for the 
%               first time.
%



% Part 3: Implement regularization with the cost function and gradients.
%
%         Hint: You can implement this around the code for
%               backpropagation. That is, you can compute the gradients for
%               the regularization separately and then add them to Theta1_grad
%               and Theta2_grad from Part 2.
%

% -------------------------------------------------------------

% =========================================================================

% Unroll gradients
grad = [Theta1_grad(:) ; Theta2_grad(:)];

end
