function [J, grad] = costFunction(theta, X, y)
%COSTFUNCTION Compute cost and gradient for logistic regression
%   J = COSTFUNCTION(theta, X, y) computes the cost of using theta as the
%   parameter for logistic regression and the gradient of the cost
%   w.r.t. to the parameters.

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta
%
% Note: grad should have the same dimensions as theta
%

% theta is 1 * n
% X is given as m * n; where the training examples are 'm' and the features are 'n' 
% so it's transpose will be n * m (features * examples)
X = X'; % n*m
H = theta' * X; % H is 1 * m
sigmoidOfH = sigmoid(H)'; %  m * 1

j1 = ( (- y') * log(sigmoidOfH)); % 1 * m

j2 = ( (1 - y') * log (1 - sigmoidOfH) ); % 1 * m

J =  (j1 - j2) / m ; % 1*m 

grad1 = (sigmoidOfH - y); %m * 1

grad = (X * grad1) / m  ;% must be n * 1

% =============================================================

end
