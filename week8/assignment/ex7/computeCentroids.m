function centroids = computeCentroids(X, idx, K)
%COMPUTECENTROIDS returs the new centroids by computing the means of the 
%data points assigned to each centroid.

%   centroids = COMPUTECENTROIDS(X, idx, K) returns the new centroids by 
%   computing the means of the data points assigned to each centroid. It is
%   given a dataset X where each row is a single data point, a vector
%   idx of centroid assignments (i.e. each entry in range [1..K]) for each
%   example, and K, the number of centroids. You should return a matrix
%   centroids, where each row of centroids is the mean of the data points
%   assigned to it.
%

% Useful variables
[m n] = size(X);

% You need to return the following variables correctly.
centroids = zeros(K, n);

% ====================== YOUR CODE HERE ======================
% Instructions: Go over every centroid and compute mean of all points that
%               belong to it. Concretely, the row vector centroids(i, :)
%               should contain the mean of the data points assigned to
%               centroid i.
%
% Note: You can use a for-loop over the centroids to compute this.
%

for clusterIndex = 1 : K
    
    %centroidIndices = centroidIndices((idx == clusterIndex) , :)
    
    %firstCentroidIndices = (idx == 1)
    %secondCentroidIndices = (idx == 2)
    %thirdCentroidIndices = (idx == 3)

    %indicesOfExamplesAssignedToCentroids1 = find(firstCentroidIndices); 
    %indicesOfExamplesAssignedToCentroids2 = find(secondCentroidIndices); 
    %indicesOfExamplesAssignedToCentroids3 = find(thirdCentroidIndices); 

    centroidIndices = find(idx == clusterIndex);
    
    centroids(clusterIndex,1:end) = mean(X(centroidIndices,:));     
end

%X1 = X(indicesOfExamplesAssignedToCentroids1, :)
%X2 = X(indicesOfExamplesAssignedToCentroids2, :)
%X3 = X(indicesOfExamplesAssignedToCentroids3, :)

%centroids = [mean(X1) ; mean(X2); mean(X3)];

% =============================================================
end

